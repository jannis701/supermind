## SuperMind
Singleplayer only in-browser-game following the game-principle of MASTERMIND® / SUPERHIRN® (by PRESSMAN TOY / INVICTA / HASBRO).

* Rules and description: https://en.wikipedia.org/wiki/Mastermind_(board_game)

Demo: https://js.uber.space/supermind

I wrote this game as an example-project for
* writing a [PWA](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps) (progressive web app)
* playing around with custom html-elements (own tags)
* playing around with touch-events for smartphones in javascript

See the section **Technical points** below.


## Deploy
The game is ready-to-run in the folder `public_html`. The file `public_html/css/style.css` is generated from `src-css/style.css` by tailwindcss (see section Develop below). All other files are source-files themselves.

To make the game installable as a [PWA](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps) (progressive web app) on smartphones, it needs to be served in a "secure context": over HTTPS or locally.

Also remember to change the version string of the app in in `public_html/js/app.js` and `public_html/current-version.json` when deploying changes!

## Develop

* Just edit files in `public_html` and `src-css`
* `npm run build:css` to generate css-styles from `src-css/style.css`
* open `public_html/index.html`

To make shure, that your changes are loaded in the browser (and not the cached files are used), do one of the following:
* update the value for `installedVersion` in `public_html/js/app.js` and `version` in `public_html/current-version.json` to a new value (both have to be identical!) OR
* remove the service-worker in the developer-tools of your browser

Eather way do an un-cached reload of the page in the browser afterwards.

### Files an folders
* `public_html`: sourcecode which can be opened and run in the browser right away
* `src-css`: tailwind-source-files to create `public_html/css/style.css`

## Technical points
### PWA (progressive web app)
This little game can be used offline on a smartphone, after it was opened the first time in the browser. Therefore it feels similar to a standalone "app" if you use the "add to Home screen" or "install app"-Feature of current browsers. This works by using an service-worker, which initializes a cache in the browser with all the needed files for the game. The service-worker also advices the browser to try to get these files from cache first, before getting them online.

To make the game a PWA we need also to offer the file `public_html/manifest.json` and an app-icon which is at least 172px x 172px.

More information about this feature can be found on https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps

### Updates for the PWA
One problem is, that once the service-worker is "installed" by the browser the files will be fetched from cache even if you are online. This is nice for performance, but it will result in not update the "app" if a new version gets deployed. Even if you refresh the page in the browser while you are online.
Therefore I built a mechanism, which fetches the (not-cached) file `public_html/current-version.json` and on success (so only if you are online) it compares the contained version-number with the version-number in the currently used script. If (and only if) this version-number differs, the service-worker will be unregistered (and the cache disabled) and the page will be reloaded (initializing the cache again with the updated files).
Useful information for this approach can be found here:
* https://love2dev.com/blog/how-to-uninstall-a-service-worker/

### Custom HTML Elements
The game uses a (simple) custom HTML-element `colored-piece`. See also
* https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_custom_elements

### Touch events on smartphones
I used the touch-events to make it even easier to choose the colors on touch-devices. See also
* https://developer.mozilla.org/en-US/docs/Web/API/Touch_events

## License and used software

The software itself is licensed unter the MIT License (see [License.txt](License.txt)).

The following software is used in the development of this project:
* tailwindcss ( https://tailwindcss.com/ )
