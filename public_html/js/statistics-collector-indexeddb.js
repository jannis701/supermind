class StatisticsCollectorIndexedDB {
    constructor(dbName, dbVersion) {
        this.db = null;
        this.storeName = "games";
        this.data = [];
        this.gameIsActivated = false;
        this.currentData = {};
        const request = window.indexedDB.open(dbName, dbVersion);
        request.onerror = (event) => {
            console.log("No access to indexDB, disabled satistics...");
        };
        request.onupgradeneeded = (event) => {
            const db = event.target.result;
            
            if (!db.objectStoreNames.contains(this.storeName)) {
                const store = db.createObjectStore(this.storeName, { keyPath: "started", autoIncrement: false });
            }
            this._loadGamesFromDb().then((data) => this.data = data);
            console.log("upgradeneeded");
        }
        request.onsuccess = (event) => {
            this.db = event.target.result;
            this._loadGamesFromDb().then((data) => this.data = data);
            console.log("dbsuccess");
        };
    }
    
    async startedNewGame(pieceCount, colorCount) {
        this.currentData = {"started": new Date(), "stopped": null, "turns": 0, "pieces": pieceCount, "colors": colorCount};
        this._addGameToDb(this.currentData);
        return 0; // gameId is not used for IndexedDB-Implementation of the storage
    }
    
    solved(gameId, turns) {
        const currentData = this.currentData;
        currentData["stopped"] = new Date();
        currentData["turns"] = turns;
        this._addGameToDb(currentData);
    }
    
    givenUp(gameId) {
        const currentData = this.currentData;
        currentData["stopped"] = new Date();
        currentData["turns"] = -1;
        this._addGameToDb(currentData);
    }
    
    _addGameToDb(obj) {
        const store = this.db?.transaction(this.storeName, "readwrite")
                .objectStore(this.storeName);
        if(!store) {
            return;
        }
        store.put(obj);
        this.data.push(obj);
    }
    
    _loadGamesFromDb() {
        return new Promise( (resolve, reject) => {
            if (this.db && this.db.objectStoreNames.contains(this.storeName)) {
                const request = this.db
                .transaction(this.storeName, "readonly")
                .objectStore(this.storeName)
                .getAll();
                request.onsuccess = (event) => {
                    resolve(event.target.result);
                };
                request.onerror = (event) => {
                    resolve(null);
                };
            }
            else {
                resolve(null);
            }
       });
    }
    
    resetStatistics() {
        this.data = [];
        return this.db?.transaction(this.storeName, "readwrite")
                .objectStore(this.storeName)
                .clear();
    }
    
    async hasStatistics() {
        return this.data.length > 0;
    }
    
    async getData(pieceCount, colorCount) {
        this.data = await this._loadGamesFromDb();
        return this.data.filter((obj) => (pieceCount === 0 || obj.pieces === pieceCount) && (colorCount === 0 || obj.colors === colorCount));
    }
}
