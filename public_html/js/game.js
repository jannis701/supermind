// definition of custom HTML-Element "colored-piece"
class ColoredPiece extends HTMLElement {
    static colorMap = ["#999","#000","#fff","#f4e500","#e32322","#2a71b0","#008e5b","#6d398b", "#f18e1c", "#c4037d", "#0fd", "#8cbb26"];
    static symbolMap = ["&starf;","&bull;","&SmallCircle;","&spadesuit;","&dagger;","&sung;","&infin;","&clubs;", "&num;", "&circleddash;", "&plusb;", "&oopf;"];
    
    constructor() {
        super();
        this.attachShadow({ mode: "open" }); // use shadow-DOM
        
        this.divElement = document.createElement("div");
        const style = document.createElement("style");
        style.textContent = `div {
            width: 100%;
            height: 100%;
            border-radius: 9999px;
            transition-property: background-color;
            transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
            transition-duration: 300ms;
            box-sizing: border-box;
            font-size: 4vmin;
            font-weight: bold;
            display: flex;
            align-items: center;
            justify-content: center;
            user-select: none;
        }`;
        this.shadowRoot.append(style, this.divElement);
    }
    
    attributeChangedCallback(name, oldValue, newValue) {
        if(oldValue === newValue) {
            return;
        }
        if(name === "color") {
            this.color = parseInt(newValue);
            this.setColor();            
        }
        if(name === "sw") {
            this.sw = (newValue === "true");
            this.setColor();
        }
        else if(name === "delay") {
            this.divElement.style.transitionDelay = `${parseInt(newValue)*150}ms`;
        }
    }
    
    setColor() {
        if(this.sw){
            this.divElement.style.backgroundColor = this.color >= 0 ? "#fff" : "none";
            this.divElement.innerHTML = this.color >= 0 ? ColoredPiece.symbolMap[this.color] : "";
        }
        else {
            this.divElement.innerHTML = "";
            this.divElement.style.backgroundColor = this.color >= 0 ? ColoredPiece.colorMap[this.color] : "none";
        }
    }
    
    static get observedAttributes() { return ["color", "delay","sw"]; }
}

class SuperMindGame {
    constructor(statisticsCollector) {
        this.statisticsCollector = statisticsCollector;
    }
    
    startNewGame(pieceCount, colorCount){
        this.turn = 0;
        this.solved = false;
        this.code = [];
        this.colorCount = colorCount;
        this.pieceCount = pieceCount;
        this.moveHistory = [];
        
        // generate code to solve
        for(let i = 0; i < this.pieceCount; i++) {
            this.code.push(Math.floor(Math.random() * this.colorCount));
        }
    }
    
    checkMove(move) {
        return !this.moveHistory.includes(move.toString());
    }
    
    play(move) {
        this.moveHistory.push(move.toString());
        if(this.turn === 0) {
            this.statisticsCollector.startedNewGame(this.pieceCount, this.colorCount).then((id) => this.currentGameId = id);
        }
        
        this.turn++;        
        const hints = new Map();
        
        // check for correct color at the correct position (black hints)
        for(let i = 0; i < this.code.length; i++) {
            if(this.code[i] === move[i]) {
                hints.set(i, 1) // Black
            }
        }
        
        // check if solved (all colors at the correct position)
        if (hints.size === this.code.length) {
            this.statisticsCollector.solved(this.currentGameId, this.turn);
            this.solved = true;
            return Array.from(hints.values());
        }
        
        // check which color-pieces of the code were set at wrong positions (white hints)
        for(let i = 0; i < this.code.length; i++) {
            // color-piece of the code has already got a black hint <-> is at the right (not at the wrong) position
            if(hints.has(i) && hints.get(i) === 1) {
                continue;
            }
            for(let j = 0; j < this.code.length; j++) {
                // color-piece of the code at position j was found at position i of the player's move
                // AND was not found yet at another position of the player's move
                // (don't give 2 white hints for the same code-piece!)
                if(move[i] === this.code[j] && !hints.has(j)) {
                    hints.set(j, 2); // White
                    break;
                }
            }
        }
        return Array.from(hints.values()).sort((a, b) => a-b); // black hints first
    }
    
    giveUpAndGetCode() {
        if(this.turn !== 0) {
            this.statisticsCollector.givenUp(this.currentGameId);
        }
        this.solved = true;
        return this.code;
    }
}

class SuperMindGameDrawer {
    constructor(rowCount = 5, pieceCount = 4, colorCount = 8, statisticsCollector=undefined) {
        this.defaultRowCount = rowCount;
        this.sw = false;
        document.getElementById("game").addEventListener("click", () => { 
            this.unselectColor();
            this.closeColorSelector();
        });
        
        // default: dummy-collecor, that does not collect any data for statistics
        this.statisticsCollector = statisticsCollector || 
            {startedNewGame(p,c){},solved(g,t){},givenUp(g){},hasStatistics(){},resetStatistics(){},getData(p,c){}}
        this.game = new SuperMindGame(statisticsCollector);
        this.startNewGame(pieceCount, colorCount);
    }
    
    startNewGame(pieceCount, colorCount) {
        this.game.startNewGame(pieceCount, colorCount);
        this.rowCount = this.defaultRowCount;
        ElementHelper.resetCache();
        this.resetBoard();
        this.closeAlert();
        this.closeSettings();
        this.closeMenu();
        ElementHelper.getElementById("statistics-piece-count").value = this.game.pieceCount;
        ElementHelper.getElementById("statistics-color-count").value = this.game.colorCount;
        ElementHelper.getElementById("button-play").disabled = false;
        ElementHelper.getElementById("button-giveup").disabled = false;
        ElementHelper.getElementById("row-spacer").scrollIntoView({block: "center", inline: "nearest"});
    }
    
    openColorSelector(selectedElementId) {
        if(this.game.solved) {
            return;
        }
        const elementToUnselect = this.unselectColor();
        if(elementToUnselect?.getAttribute("id") === selectedElementId){
            this.closeColorSelector();
            return;
        }

        ElementHelper.getElementById("color-selector").classList.replace("hidden", "grid");
        ElementHelper.getElementById(selectedElementId).classList.add("selected");
    }
    
    closeColorSelector() {
        ElementHelper.getElementById("color-selector").classList.replace("grid", "hidden");
    }

    unselectColor(){
        const elementToUnselect = document.querySelector("colored-piece.selected:not(.color-selector-item)");
        elementToUnselect?.classList.remove("selected");
        return elementToUnselect;
    }
    
    selectColor(color) {
        const selectedElement = this.unselectColor();
        selectedElement?.setAttribute("color", color);
        this.closeColorSelector();
        
        // check if this combination was already played
        const move = this.getCurrentMove();
        ElementHelper.getElementById("button-play").disabled = !this.game.checkMove(move);
    }

    getCurrentMove() {   
        const move = [];
        for(let i = 0; i < this.game.pieceCount; i++) {
            const colorI = ElementHelper.getElementById(`input-0-${i}`).getAttribute("color");
            move.push(parseInt(colorI));
        }
        return move;
    }
    
    play() {
        const move = this.getCurrentMove();
        const hints = this.game.play(move);
        
        const currentRow = this.game.turn - 1;
        
        // scroll current row into view
        ElementHelper.getElementById(`row-${currentRow}`, false).scrollIntoView({block: "center", inline: "nearest", behaviour: "smooth"});
        
        // draw move and colors to current row
        move.forEach((m, i) => ElementHelper.getElementById(`code-${currentRow}-${i}`, false).setAttribute("color", m) );
        hints.forEach((h, i) => ElementHelper.getElementById(`hint-${currentRow}-${i}`, false).setAttribute("color", h) );
        
        ElementHelper.getElementById("button-play").disabled = true;
        if(this.game.solved) {
            setTimeout(() => {
                ElementHelper.getElementById(`row-${currentRow}`, false).classList.add("border-2", "border-green-600");
                ElementHelper.getElementById("button-giveup").disabled = true;
            }, 1000);
        } else if (this.game.turn >= this.rowCount) {
            this.addTurnRow(this.rowCount);
            this.rowCount++;
        }
    }
    
    async openStatistics() {
        this.closeAlert();
        this.closeMenu();
        this.closeSettings();
        const hasStatistics = await this.statisticsCollector.hasStatistics();
        if(!hasStatistics) {
            this.openAlert("Noch keine Daten vorhanden");
            return;
        }
        
        const pieceCount = Number(ElementHelper.getElementById("statistics-piece-count").value);
        const colorCount = Number(ElementHelper.getElementById("statistics-color-count").value);
        const statisticsData = await this.statisticsCollector.getData(pieceCount, colorCount);
        const gamesTotalCount = statisticsData.length;
        const gamesSolvedTotalCount = (statisticsData.filter((obj) => obj.turns > 0)).length;
        const gamesSolvedCounts = {"-1":0, "0":0};
        statisticsData.forEach((obj) => {
            gamesSolvedCounts[obj.turns] = (gamesSolvedCounts[obj.turns] ?? 0) + 1;
        });
        
        // console.log(gamesSolvedCounts, gamesSolvedTotalCount);
        
        const countMap = {"0": "any"};
        ElementHelper.getElementById("statistics-games-header").textContent = `pieces: ${countMap[pieceCount] ?? pieceCount} / colors: ${countMap[colorCount] ?? colorCount}`;
        
        if(gamesTotalCount > 0) {
            ElementHelper.getElementById("statistics-games-no-data").classList.replace("block", "hidden");
            ElementHelper.getElementById("statistics-games").classList.replace("hidden", "block");
            let gamesSolvedPercent = gamesSolvedTotalCount > 0 ? (gamesSolvedTotalCount/gamesTotalCount*100).toFixed(0) : 0;
            ElementHelper.getElementById("statistics-games-solved").innerHTML = gamesSolvedPercent >= 10 ? `${gamesSolvedTotalCount} (${gamesSolvedPercent}%)` : gamesSolvedPercent === 0 ? "" : gamesSolvedTotalCount;
            ElementHelper.getElementById("statistics-games-solved").style.width = `${gamesSolvedPercent}%`;
            let gamesGivenupPercent = gamesSolvedCounts[-1] > 0 ? (gamesSolvedCounts[-1]/gamesTotalCount*100).toFixed(0) : 0;
            ElementHelper.getElementById("statistics-games-givenup").innerHTML = gamesGivenupPercent >= 10 ? `${gamesSolvedCounts[-1]} (${gamesGivenupPercent}%)` : gamesGivenupPercent === 0 ? "" : gamesSolvedCounts[-1];
            ElementHelper.getElementById("statistics-games-givenup").style.width = `${gamesGivenupPercent}%`;
            let gamesNotfinishedPercent = gamesSolvedCounts[0] > 0 ? (gamesSolvedCounts[0]/gamesTotalCount*100).toFixed(0) : 0;
            ElementHelper.getElementById("statistics-games-not-finished").innerHTML = gamesNotfinishedPercent >= 10 ? `${gamesSolvedCounts[0]} (${gamesNotfinishedPercent}%)` : gamesNotfinishedPercent === 0 ? "" : gamesSolvedCounts[0];
            ElementHelper.getElementById("statistics-games-not-finished").style.width = `${gamesNotfinishedPercent}%`;
        }
        else {
            ElementHelper.getElementById("statistics-games").classList.replace("block", "hidden");
            ElementHelper.getElementById("statistics-games-no-data").classList.replace("hidden", "block");
        }
        
        
        ElementHelper.getElementById("statistics-turns-needed-plot").replaceChildren(); //empty div
        if(pieceCount !== 0 && colorCount !== 0 && gamesTotalCount > 0) {
            ElementHelper.getElementById("statistics-turns-needed").classList.replace("hidden", "block");
            const colorMap = {"-1": "bg-red-600", "0": "bg-gray-300"};
            const maxHeight = 200;
            for (let c in gamesSolvedCounts) {
                if(gamesSolvedCounts[c] === 0) {
                    continue;
                }
                const displayEl = ElementHelper.createElement("div",{}, ["grow", "text-center", "break-words", c <= 0 ? "order-first":"order-0"]);
                const descrEl = ElementHelper.createElement("div", {}, [], `${gamesSolvedCounts[c]} (${(gamesSolvedCounts[c]/gamesTotalCount*100).toFixed(0)}%)`);
                const barEl = ElementHelper.createElement("div",{"style": `height: ${(gamesSolvedCounts[c]/gamesTotalCount*maxHeight).toFixed(0)}px`}, [colorMap[c] ?? "bg-green-600"]);
                const axisEl = ElementHelper.createElement("div", {}, [], `${c <= 0 ? "-" : c}`);
                displayEl.appendChild(descrEl);
                displayEl.appendChild(barEl);
                displayEl.appendChild(axisEl);
                ElementHelper.getElementById("statistics-turns-needed-plot").appendChild(displayEl);
            }
        }
        else {
            ElementHelper.getElementById("statistics-turns-needed").classList.replace("block", "hidden");
        }
        ElementHelper.getElementById("statistics-box").classList.replace("hidden", "block");
    }
    
    closeStatistics() {
        ElementHelper.getElementById("statistics-box").classList.replace("block", "hidden");
    }
    
    resetStatistics() {
        this.openAlert("Reset all statistics?", () => {
            this.statisticsCollector.resetStatistics();
            this.closeStatistics();
            this.openStatistics();
        });
        
    }
    
    openSettings() {
        this.closeAlert();
        this.closeMenu();
        this.closeStatistics();
        ElementHelper.getElementById("settings-piece-count").value = this.game.pieceCount;
        ElementHelper.getElementById("settings-color-count").value = this.game.colorCount;
        ElementHelper.getElementById("settings-box").classList.replace("hidden", "block");
    }
    
    closeSettings() {
        ElementHelper.getElementById("settings-box").classList.replace("block", "hidden");
    }
    
    openMenu() {
        this.closeAlert();
        this.closeSettings();
        this.closeStatistics();
        ElementHelper.getElementById("menu-box").classList.replace("hidden", "flex");
        ElementHelper.getElementById("button-menu").innerHTML = "&times;";
    }
    
    closeMenu() {
        ElementHelper.getElementById("menu-box").classList.replace("flex", "hidden");
        ElementHelper.getElementById("button-menu").innerHTML = "&equiv;";
    }
    
    toggleSw() {
        const swItems = document.querySelectorAll(`[sw='${this.sw.toString()}']`);
        this.sw = !this.sw;
        ElementHelper.getElementById("button-sw").innerHTML = this.sw ? "s/w: &check;" : "s/w: &cross;";
        document.getElementById("game").classList.replace(`sw-${(!this.sw).toString()}`,`sw-${this.sw.toString()}`);
        
        swItems.forEach((swItem) => swItem.setAttribute("sw", this.sw));
    }
    
    toggleMenu() {
        ElementHelper.getElementById("menu-box").classList.contains("hidden") ? this.openMenu() : this.closeMenu();
    }
    
    saveSettings() {
        const pieceCount = Number(ElementHelper.getElementById("settings-piece-count").value);
        const colorCount = Number(ElementHelper.getElementById("settings-color-count").value);
        this.requestNewGame(pieceCount, colorCount);
    }
    
    openAlert(msg, fnc=null) {
        this.closeSettings();
        this.closeMenu();
        ElementHelper.getElementById("alert-box-msg").innerHTML = msg;
        if(fnc !== null) {
            ElementHelper.getElementById("alert-box-button-cancel").classList.replace("hidden", "block");
            ElementHelper.getElementById("alert-box-button-ok").onclick = fnc;
        }
        else {
            ElementHelper.getElementById("alert-box-button-cancel").classList.replace("block", "hidden");
            ElementHelper.getElementById("alert-box-button-ok").onclick = this.closeAlert;
        }
        ElementHelper.getElementById("alert-box").classList.replace("hidden", "block");
    }
    
    closeAlert() {
        ElementHelper.getElementById("alert-box").classList.replace("block", "hidden");
    }
    
    giveUp() {
        this.openAlert("Give up?",() => {
            const codeSolution = this.game.giveUpAndGetCode();
            const solutionRow = this.rowCount - 1;
            codeSolution.forEach((c, i) => ElementHelper.getElementById(`code-${solutionRow}-${i}`, false).setAttribute("color", c));
            ElementHelper.getElementById(`row-${solutionRow}`).classList.add("border-2", "border-red-600");
            ElementHelper.getElementById(`row-${solutionRow}`).scrollIntoView({block: "center", inline: "nearest", behaviour: "smooth"});
            ElementHelper.getElementById("button-play").disabled = true;
            ElementHelper.getElementById("button-giveup").disabled = true;
            this.closeAlert();
        });
    }
    
    requestNewGame(pieceCount=null, colorCount=null) {
        if(this.game.solved) {
            this.startNewGame(pieceCount ?? this.game.pieceCount, colorCount ?? this.game.colorCount);
        }
        else {
            this.openAlert("Quit the current game?", () => {
                this.startNewGame(pieceCount ?? this.game.pieceCount, colorCount ?? this.game.colorCount);
            });
        }
    }

    resetBoard() {
        ElementHelper.getElementById("turn-rows").replaceChildren(); //empty div
        for(let i = 0; i < this.defaultRowCount; i++) {
            this.addTurnRow(i);
        }
        
        const codesInput = ElementHelper.getElementById("codes-input");
        codesInput.replaceChildren(); // empty div
        codesInput.append(this.getPieces(0, "input", 0, true));
        
        const colorSelector = ElementHelper.getElementById("color-selector");
        colorSelector.replaceChildren(); //empty div
        for(let i = 0; i < this.game.colorCount; i++) {
            colorSelector.append(ElementHelper.createElement("colored-piece", {"color": i, "onClick": `gameDrawer.selectColor(${i})`, "sw": this.sw.toString()}, ["color-selector-item"]));
        }
    }
    
    addTurnRow(currentRow) {
        const turnRowTemplate = ElementHelper.createElement("div", {"id": `row-${currentRow}`}, ["flex", "game-row", "min-[250px]:p-2", "p-1", "items-center", "gap-3", "rounded", "my-2", "bg-gray-300", "border-0"]);
        turnRowTemplate.append(ElementHelper.createElement("div", {}, ["text-center"], `${currentRow+1}`));
        turnRowTemplate.append(this.getPieces(currentRow));
        const hintsField = ElementHelper.createElement("div", {}, ["grid", "grid-rows-2", "grid-flow-col", "justify-items-center", "items-center", "gap-1"]);
        for(let i = 0; i < this.game.pieceCount; i++) {
            hintsField.append(ElementHelper.createElement("colored-piece", {"id": `hint-${currentRow}-${i}`, "delay": `${i+this.game.pieceCount}`, "sw": this.sw.toString()}, ["hint-piece"]));
        }
        turnRowTemplate.append(hintsField);
        
        ElementHelper.getElementById("turn-rows").prepend(turnRowTemplate);
    }
    
    getPieces(currentRow, idPrefix="code", color=undefined, clickable = false) {
        const piecesField = ElementHelper.createElement("div", {}, ["grid", "grid-rows-1", "grid-flow-col", "justify-items-center", "items-center", "gap-2"]);
        for(let i = 0; i<this.game.pieceCount; i++) {
            const pieceElem = ElementHelper.createElement("colored-piece", {"id": `${idPrefix}-${currentRow}-${i}`, "color": `${color}`, "delay": `${clickable ? 0 : i}`, "sw": this.sw.toString()});
            if(clickable) {
                pieceElem.addEventListener("click", (ev) => { 
                    gameDrawer.openColorSelector(`${idPrefix}-${currentRow}-${i}`);
                    ev.stopPropagation();
                });
                pieceElem.addEventListener("touchstart", (ev) => {
                    gameDrawer.openColorSelector(`${idPrefix}-${currentRow}-${i}`);   
                    ev.stopPropagation();
                }, false );
                pieceElem.addEventListener("touchend", (ev) => {
                        const el = document.elementFromPoint(ev.changedTouches[0].clientX, ev.changedTouches[0].clientY);
                        if(el.classList.contains("color-selector-item")) {
                            el.classList.remove("selected");
                            this.selectColor(el.getAttribute("color"));
                        }
                        ev.preventDefault();
                    }, false );
                pieceElem.addEventListener("touchmove", (ev) => { 
                    ev.preventDefault();
                    const el = document.elementFromPoint(ev.changedTouches[0].clientX, ev.changedTouches[0].clientY);
                    const csItems = document.querySelectorAll("colored-piece.color-selector-item.selected");
                    csItems.forEach((csItem) => {
                        if(csItem !== el) {
                            csItem.classList.remove("selected");
                        }
                    });
                    if(el.classList.contains("color-selector-item") && !el.classList.contains("selected")) {
                        el.classList.add("selected");
                    }
                });
            }
            piecesField.append(pieceElem);
        }
        return piecesField;
    }
}
