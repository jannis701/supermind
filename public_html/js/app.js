const installedVersion = "1.3.7";
const dbVersion = 1;

var gameDrawer = null;
document.addEventListener("DOMContentLoaded", function (event) {
    const ROW_COUNT = 7;
    const PIECE_COUNT = 4;
    const COLOR_COUNT = 8;
    document.getElementById("noscript").classList.add("hidden");
    // define custom element "colored-piece" (definition see game.js)
    customElements.define("colored-piece", ColoredPiece);
    statisticsCollector = new StatisticsCollectorIndexedDB("mmStorage", dbVersion);
    gameDrawer = new SuperMindGameDrawer(ROW_COUNT,PIECE_COUNT, COLOR_COUNT, statisticsCollector);
});

// SERVICE-WORKER-STUFF for PWA

async function unregisterServiceWorker() {
    if ("serviceWorker" in navigator) {
        try {
            const registrations = await navigator.serviceWorker.getRegistrations();
            await Promise.all(registrations.map(registration => registration.unregister()));
            window.location.reload();
        } catch (error) {
            console.error(`Unregistration failed with ${error}`);
        }
    } else {
        console.log("Service Worker not supported");
    }
};

async function registerServiceWorker() {
    if ("serviceWorker" in navigator) {
        try {
            const currentVersion = await fetch("current-version.json?noCache="+Math.random());
            if(currentVersion?.ok && (await currentVersion.json()).version !== installedVersion) {
                return unregisterServiceWorker();
            }
        }
        catch (error) {
            console.log("Working offline... ignoring check for new version", error);
        }
        try {
            const reg = await navigator.serviceWorker.register("service-worker.js", {scope: "./"});
        } catch (error) {
            console.error(`Registration failed with ${error}`);
        }
    } else {
        console.log("Service Worker not supported");
    }
};

registerServiceWorker();

var installPrompt = null;
window.addEventListener("beforeinstallprompt", (event) => {
    event.preventDefault();
    const installButton = document.getElementById("button-install-app");
    installPrompt = event;
    installButton.classList.remove("hidden");

    installButton.addEventListener("click", async () => {
        if (!installPrompt) {
            return;
        }
        const installButton = document.getElementById("button-install-app");
        const result = await installPrompt.prompt();
        console.log(`Install prompt was: ${result.outcome}`);
        installPrompt = null;
        installButton.classList.add("hidden");
    });
});
