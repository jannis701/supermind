class ElementHelper {
    static elementCache = new Map();
    
    static resetCache() {   
        ElementHelper.elementCache = new Map();
    }
    
    static getElementById(elementId, cache=true) {
        if(!cache) {
            return document.getElementById(elementId);
        }
        if(!ElementHelper.elementCache.has(elementId)) {
            ElementHelper.elementCache.set(elementId, document.getElementById(elementId))
        }
        return ElementHelper.elementCache.get(elementId);
    }
    
    static modifyElementById(elementId, attributes = {}, classList = [], textContent = null) {
        element = ElementHelper.getElementById(elementId);
        return ElementHelper.modifyElement(element, attributes, classList, textContent);
    }
    
    static createElement(type, attributes = {}, classList = [], textContent = null) {
        var element = document.createElement(type);
        return ElementHelper.modifyElement(element, attributes, classList, textContent);
    }

    static modifyElement(element, attributes = {}, classList = [], textContent = null) {
        for (let key in attributes) {
            element.setAttribute(key, attributes[key]);
        }
        for (let c in classList) {
            if (classList[c]) {
                element.classList.add(classList[c]);
            }
        }
        if (textContent !== null) {
            element.textContent = textContent;
        }
        return element;
    }
}
