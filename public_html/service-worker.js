const activeCacheName = "mmCache";
const precachedResources = ["./", "./index.html", "./js/app.js", "./js/game.js", "./js/element-helper.js", "./js/statistics-collector-indexeddb.js", "./css/style.css", "./images/icon72x72.png", "./images/icon180x180.png", "./images/icon512x512.png"];

async function precache() {
  const cache = await caches.open(activeCacheName);
  return cache.addAll(precachedResources);
}

self.addEventListener("install", (event) => {
  event.waitUntil(precache());
}); 


async function cacheFirst(request) {
  const cachedResponse = await caches.match(request);
  if (cachedResponse) {
    console.log("Found response in cache:", cachedResponse);
    return cachedResponse;
  }
  return fetch(request);
}

self.addEventListener("fetch", (event) => {
    event.respondWith(cacheFirst(event.request));
});
